<?php

namespace App\Form;

use App\Entity\Depot;
use App\Form\ApplicationType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class DepotType extends ApplicationType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('monDepot', MoneyType::class,[
                'label' => 'Montant'
            ])
            ->add('observ',TextareaType::class,[
                'label' => 'Observation',
                'required' => false
            ])
            ->add('photo', FileType::class,[
                'required' => false,
                'mapped' => false,
                // 'multiple'=>true,
                'attr' => [
                    'class' => 'image-preview'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Depot::class,
        ]);
    }
}
