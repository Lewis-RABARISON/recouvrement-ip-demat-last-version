<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /*public function agentAmountHistoric()
    {
        return $this->createQueryBuilder('u')
            ->addSelect('u.nom','u.prenom','u.numCom','SUM(r.monRecu) AS monRecu','SUM(depot.monDepot) AS monDepot','SUM(depense.monDep) AS monDep')
            ->leftjoin('u.rechargements','r')
            ->leftjoin('u.depenses','depense')
            ->leftjoin('u.depots','depot')
            ->andWhere('r.isValid = :isValid')
            ->setParameter('isValid', true)
            ->orderBy('u.nom', 'ASC')
            ->groupby('u.numCom')
            ->getQuery()
            ->getResult()
        ;
    }*/

    public function agentAmountHistoric(){
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            "SELECT user.nom, user.prenom,user.numCom,
            (SELECT SUM(rechargement.monRecu) 
              FROM App\Entity\Rechargement rechargement WHERE rechargement.agent=user.id AND rechargement.isValid = :isValid
            ) 
            AS monRecu,
            (SELECT SUM(depot.monDepot) FROM App\Entity\Depot depot WHERE depot.agent=user.id) AS monDepot,
            (SELECT SUM(depense.monDep) FROM App\Entity\Depense depense WHERE depense.agent=user.id) AS monDep 
            FROM App\Entity\User user 
            WHERE user.roles NOT LIKE :role
            ORDER BY user.nom"
        )->setParameter('isValid', true)->setParameter('role', '%ROLE_ADMIN%');

        return $query->getResult();
    }

    public function amountAgent($agent_email){
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            "SELECT user.nom,
            (SELECT SUM(rechargement.monRecu) 
              FROM App\Entity\Rechargement rechargement WHERE rechargement.agent=user.id AND rechargement.isValid = :isValid
            ) 
            AS monRecu,
            (SELECT SUM(depot.monDepot) FROM App\Entity\Depot depot WHERE depot.agent=user.id) AS monDepot,
            (SELECT SUM(depense.monDep) FROM App\Entity\Depense depense WHERE depense.agent=user.id) AS monDep 
            FROM App\Entity\User user 
            WHERE user.roles NOT LIKE :role AND user.email = :email
            ORDER BY user.nom
            "
        )->setParameter('isValid', true)
         ->setParameter('role', '%ROLE_ADMIN%')
         ->setParameter('email', $agent_email);

        return $query->getResult();
    }
    

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
