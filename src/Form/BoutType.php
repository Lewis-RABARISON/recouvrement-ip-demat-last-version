<?php

namespace App\Form;

use App\Entity\Bout;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class BoutType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('code', IntegerType::class,[
                'label'=> 'Code'
            ])
            ->add('nom', TextType::class,[
                'label'=>'Nom'
            ])
            ->add('nRcs',IntegerType::class,[
                'label'=>'N° RCS'
            ])
            ->add('numFixe', TextType::class,[
                'label'=> 'Numéro Fixe',
                'required'=> false
            ])
            ->add('numMobile', TextType::class,[
                'label'=>'Numéro Mobile',
                'required'=> false
                ])
            ->add('numTelecopie', TextType::class,[
                'label'=>'Numéro Télécopie',
                'required'=> false
                ])
            ->add('adresse', TextType::class,[
                'label'=>'Adresse'
                ])
            ->add('codePost', IntegerType::class,[
                'label'=>'Code Postal'
            ])
            ->add('ville', TextType::class,[
                'label'=>'Ville'
                ])
            ->add('pays', TextType::class,[
                'label'=>'Pays'
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Bout::class,
        ]);
    }
}
