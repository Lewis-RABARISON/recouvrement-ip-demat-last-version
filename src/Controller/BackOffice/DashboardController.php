<?php

namespace App\Controller\BackOffice;

use App\Repository\UserRepository;
use App\Repository\DepotRepository;
use App\Repository\SoldesRepository;
use App\Repository\DepenseRepository;
use App\Repository\RechargementRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DashboardController extends AbstractController
{
    /**
     * @Route("/tableau-de-bord", name="dashboard")
     * @IsGranted("ROLE_ADMIN", message="Vous ne pouvez pas accéder sur cette url, sera réserve à l’Administrateur!")
     */
    public function index(
                          DepotRepository $depotRepository,
                          DepenseRepository $depenseRepository,
                          RechargementRepository $rechargementRepository): Response
    {
        if (!$this->getUser())
        {
            return $this->redirectToRoute('app_login');
        }

        /* compte journalier  */
        $compte_journalier_rechargement = $rechargementRepository->CompteJournalierRechargement();
        $compte_journalier_depose = $depotRepository->CompteJournalierDeposer();
        $compte_depenser_journalier = $depenseRepository->CompteJournalierDepenser();
        // dd($compte_depenser_journalier);

        /* compte mensuel */
        $compte_mensuel_rechargement = $rechargementRepository->CompteMensuelRechargement();
        $compte_mensuel_depose = $depotRepository->CompteMensuelDepot();
        $compte_mensuel_depenser = $depenseRepository->CompteMensuelDepenser();

        /* compte annuelle */
        $compte_annuelle_rechargement = $rechargementRepository->CompteAnnulleRechargement();
        $compte_annuelle_depot = $depotRepository->CompteAnnuelleDepot();
        $Compte_annuelle_depenser = $depenseRepository->CompteAnnuelleDepenser();


        return $this->render('BackOffice/dashboard/index.html.twig', [
            'compte_journalier_rechargement' => $compte_journalier_rechargement,
            'compte_journalier_depose' => $compte_journalier_depose,
            'compte_depenser_journalier'=>$compte_depenser_journalier,
            'compte_mensuel_rechargement' => $compte_mensuel_rechargement,
            'compte_mensuel_depose' => $compte_mensuel_depose,
            'compte_mensuel_depenser'=>$compte_mensuel_depenser,
            'compte_annuelle_rechargement'=>$compte_annuelle_rechargement,
            'compte_annuelle_depot'=> $compte_annuelle_depot,
            'Compte_annuelle_depenser' => $Compte_annuelle_depenser
        ]);
    }
}
