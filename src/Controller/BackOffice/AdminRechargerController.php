<?php

namespace App\Controller\BackOffice;

use DateTime;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Repository\SoldesRepository;
use App\Repository\RechargementRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminRechargerController extends AbstractController
{
    private $userRepository;
    private $rechargementRepository;
    private $soldesRepository;

    public function __construct(UserRepository $userRepository, 
                                RechargementRepository $rechargementRepository, 
                                SoldesRepository $soldesRepository){
        $this->userRepository = $userRepository;
        $this->rechargementRepository = $rechargementRepository;
        $this->soldesRepository = $soldesRepository;
    }

       /**
     * @Route("/rechargement-du-jour", name="recharger_jour")
     * @IsGranted("ROLE_ADMIN", message="Vous ne pouvez pas accéder sur cette url, sera réserve à l’Administrateur!")
     */
    public function recu(): Response
    {
        if (!$this->getUser())
        {
            return $this->redirectToRoute('app_login');
        }

        $compteurR = $this->rechargementRepository->findBy([
            'date'=>new DateTime(), 
            "isValid" => 0
        ]);

        $result = count($compteurR) ?? 0;
        // dd($result);

        return $this->render('BackOffice/admin_recharger/index.html.twig', [
            'users' => $this->userRepository->findAll(),
            // $admin => $userRepository->findBy(['agent', $user->getPrenom()]),
            'rechargements'=>$this->rechargementRepository->findBy([
                'date'=>new DateTime()
            ])
        ]);
    }

    /**
    * @Route("/suivie-collecte", name="suivie_collecte")
    */
    public function SuivieCollecte()
    {
        // $agents_amount_historic = $this->soldesRepository->agentAmountHistoric();
        $agents_amount_historic = $this->userRepository->agentAmountHistoric();

        return $this->render('BackOffice/admin_recharger/suivie.html.twig',compact('agents_amount_historic'));
    }
}
