<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220127111809 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE bout (id INT AUTO_INCREMENT NOT NULL, code VARCHAR(30) DEFAULT NULL, nom VARCHAR(100) DEFAULT NULL, n_rcs VARCHAR(30) DEFAULT NULL, num_fixe VARCHAR(30) DEFAULT NULL, num_mobile VARCHAR(30) DEFAULT NULL, num_telecopie VARCHAR(20) DEFAULT NULL, adresse VARCHAR(100) DEFAULT NULL, code_post VARCHAR(100) DEFAULT NULL, ville VARCHAR(50) DEFAULT NULL, pays VARCHAR(100) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE course (id INT AUTO_INCREMENT NOT NULL, agent_id INT NOT NULL, date DATE NOT NULL, INDEX IDX_169E6FB93414710B (agent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE depense (id INT AUTO_INCREMENT NOT NULL, agent_id INT NOT NULL, motifs VARCHAR(100) NOT NULL, mon_dep DOUBLE PRECISION NOT NULL, date DATE NOT NULL, preuve VARCHAR(50) DEFAULT NULL, INDEX IDX_340597573414710B (agent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE depot (id INT AUTO_INCREMENT NOT NULL, agent_id INT NOT NULL, date DATE NOT NULL, mon_depot DOUBLE PRECISION NOT NULL, INDEX IDX_47948BBC3414710B (agent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rechargement (id INT AUTO_INCREMENT NOT NULL, bout_id INT NOT NULL, agent_id INT NOT NULL, date DATE NOT NULL, mon_recu DOUBLE PRECISION NOT NULL, obesr VARCHAR(100) DEFAULT NULL, INDEX IDX_479F0C5034887B6B (bout_id), INDEX IDX_479F0C503414710B (agent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE soldes (id INT AUTO_INCREMENT NOT NULL, agent_id INT NOT NULL, montant DOUBLE PRECISION DEFAULT NULL, mont_recu DOUBLE PRECISION DEFAULT NULL, mont_depo DOUBLE PRECISION DEFAULT NULL, mont_depens DOUBLE PRECISION DEFAULT NULL, INDEX IDX_C8BEAA733414710B (agent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(50) DEFAULT NULL, prenom VARCHAR(50) DEFAULT NULL, email VARCHAR(50) NOT NULL, tel VARCHAR(15) DEFAULT NULL, password VARCHAR(255) NOT NULL, picture VARCHAR(50) DEFAULT NULL, roles JSON DEFAULT NULL, num_com VARCHAR(10) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE course ADD CONSTRAINT FK_169E6FB93414710B FOREIGN KEY (agent_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE depense ADD CONSTRAINT FK_340597573414710B FOREIGN KEY (agent_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE depot ADD CONSTRAINT FK_47948BBC3414710B FOREIGN KEY (agent_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE rechargement ADD CONSTRAINT FK_479F0C5034887B6B FOREIGN KEY (bout_id) REFERENCES bout (id)');
        $this->addSql('ALTER TABLE rechargement ADD CONSTRAINT FK_479F0C503414710B FOREIGN KEY (agent_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE soldes ADD CONSTRAINT FK_C8BEAA733414710B FOREIGN KEY (agent_id) REFERENCES user (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE rechargement DROP FOREIGN KEY FK_479F0C5034887B6B');
        $this->addSql('ALTER TABLE course DROP FOREIGN KEY FK_169E6FB93414710B');
        $this->addSql('ALTER TABLE depense DROP FOREIGN KEY FK_340597573414710B');
        $this->addSql('ALTER TABLE depot DROP FOREIGN KEY FK_47948BBC3414710B');
        $this->addSql('ALTER TABLE rechargement DROP FOREIGN KEY FK_479F0C503414710B');
        $this->addSql('ALTER TABLE soldes DROP FOREIGN KEY FK_C8BEAA733414710B');
        $this->addSql('DROP TABLE bout');
        $this->addSql('DROP TABLE course');
        $this->addSql('DROP TABLE depense');
        $this->addSql('DROP TABLE depot');
        $this->addSql('DROP TABLE rechargement');
        $this->addSql('DROP TABLE soldes');
        $this->addSql('DROP TABLE user');
    }
}
