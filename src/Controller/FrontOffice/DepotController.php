<?php

namespace App\Controller\FrontOffice;

use App\Entity\Depot;
use App\Entity\Soldes;
use App\Form\DepotType;
use App\Repository\UserRepository;
use App\Repository\DepotRepository;
use App\Repository\SoldesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DepotController extends AbstractController
{

    /**
     * @Route("/faire-depot", name="montant_depot")
     * @IsGranted("ROLE_AGENT", message="Vous ne pouvez pas accéder sur cette url, sera réserve à l’agent!")
     */
    public function depot(Request $request, EntityManagerInterface $manager,
                          SoldesRepository $soldesRepository, UserRepository $userRepository): Response
    {
        if (!$this->getUser())
        {
            return $this->redirectToRoute('app_login');
        }

        
        $user = $this->getUser();
        $agent_email = $user->getEmail();
        // dump($userRepository->amountAgent($agent_email));
       
        $depot = new Depot();

        $form = $this->createForm(DepotType::class, $depot);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $depot->setAgent($user);

            $file = $depot->getPhoto();
            $file = $form->get('photo')->getData();

            If ($file) {
            $fileName = md5(uniqid()).'.'.$file->guessExtension();
            // $entityManager = $this->getDoctrine()->getManager();
            $file->move($this->getParameter('upload_directory'), $fileName);
            $depot->setPhoto($fileName); 
            }
            $manager->persist($depot);

            $solde = new Soldes();
            $montant_depot = $form->get("monDepot")->getData();
            // $montant = $soldesRepository->findBy(['agent' => $user],['id' => 'DESC']);
            $soldes_agent = $userRepository->amountAgent($agent_email);

            $mont_recu =  $soldes_agent[0]['monRecu'] != null ? $soldes_agent[0]['monRecu'] : 0;
            $mont_depot =  $soldes_agent[0]['monDepot'] != null ? $soldes_agent[0]['monDepot'] : 0;
            $mont_depense = $soldes_agent[0]['monDep'] != null ? $soldes_agent[0]['monDep'] : 0;

                /*if(count($montant)){
                    $montant = $montant[0]->getMontant();
                } else{
                    $montant = 0;
                }*/

            $montant = $mont_recu - $mont_depot - $mont_depense;  

                if($montant < $montant_depot){
                    $this->addFlash("danger","Votre dépôt doit etre inferieur au solde");
    
                    return $this->redirectToRoute("montant_depot");
                }

                $montant -= $montant_depot ;
                
                $solde->setMontDepo($montant_depot);
                $solde->setAgent($user);
                $solde->setMontant($montant);
                $manager->persist($solde);

                $manager->flush();

            return $this->redirectToRoute('home');
        }

        
        return $this->render('FrontOffice/depot/index.html.twig',[
            'form'=>$form->createView()
        ]);
    }
}
