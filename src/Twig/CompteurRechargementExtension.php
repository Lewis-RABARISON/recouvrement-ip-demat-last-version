<?php

namespace App\Twig;

use DateTime;
use Twig\TwigFilter;
use Twig\TwigFunction;
use Twig\Extension\AbstractExtension;
use App\Repository\RechargementRepository;

class CompteurRechargementExtension extends AbstractExtension
{
    private $rechargementRepository;
        public function __construct(RechargementRepository $rechargementRepository){
            $this->rechargementRepository = $rechargementRepository;
        }

    // public function getFilters(): array
    // {
    //     return [
    //         // If your filter generates SAFE HTML, you should add a third
    //         // parameter: ['is_safe' => ['html']]
    //         // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
    //         new TwigFilter('filter_name', [$this, 'doSomething']),
    //     ];
    // }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('compteurR', [$this, 'compteurRechargement']),
        ];
    }

    public function compteurRechargement()
    {
        $compteurR = $this->rechargementRepository->findBy([
            'date'=>new DateTime(), 
            "isValid" => 0
        ]);

        $result = count($compteurR) ?? 0;

        return $result;
    }
}
