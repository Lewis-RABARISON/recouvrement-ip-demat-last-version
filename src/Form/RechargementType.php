<?php

namespace App\Form;

use App\Entity\Bout;
use App\Entity\Rechargement;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class RechargementType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('monRecu', MoneyType::class,[
                'label'=>'Montant'
            ])
            ->add('obesr', TextareaType::class,[
                'label' => 'Observation',
                'required' => false
            ])
            ->add('bout', EntityType::class,[
                'class' => Bout::class,
                'choice_label' => 'nom',
                'label'=>'Boutique',
                'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('b')
                        ->orderBy('b.nom','ASC');
                },
                'attr' => [
                    'class'=>'select-bout'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Rechargement::class,
        ]);
    }
}
