<?php

namespace App\Controller\FrontOffice;

use App\Entity\Soldes;
use App\Entity\Depense;
use App\Form\DepenseType;
use App\Repository\UserRepository;
use App\Repository\SoldesRepository;
use App\Repository\DepenseRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DepenseController extends AbstractController
{
    /**
     * @Route("/dépense", name="depense")
     * @IsGranted("ROLE_AGENT", message="Vous ne pouvez pas accéder sur cette url, sera réserve à l’agent!")
     */
    public function index(DepenseRepository $depenseRepository): Response
    {
        if (!$this->getUser())
        {
            return $this->redirectToRoute('app_login');
        }

        $user = $this->getUser();
        return $this->render('FrontOffice/depense/index.html.twig', [
            'depenses' => $depenseRepository->findBy(['agent' => $user],['id' => 'DESC']),
        ]);
    }

        /**
     * @Route("/solde-dépense", name="solde_depende")
     * @IsGranted("ROLE_AGENT", message="Vous ne pouvez pas accéder sur cette url, sera réserve à l’agent!")
     */
    public function depense(Request $request,EntityManagerInterface $manager, 
                            SoldesRepository $soldesRepository, UserRepository $userRepository)
    {
        if (!$this->getUser())
        {
            return $this->redirectToRoute('app_login');
        }

        $user = $this->getUser();
        $agent_email = $user->getEmail();
        $depense = new Depense();
        
        $form = $this->createForm(DepenseType::class, $depense);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $depense->setAgent($user);

            $file = $depense->getPreuve();
            $file = $form->get('preuve')->getData();
            $fileName = md5(uniqid()).'.'.$file->guessExtension();
            // $entityManager = $this->getDoctrine()->getManager();
            $file->move($this->getParameter('upload_directory'), $fileName);
            $depense->setPreuve($fileName); 

            $manager->persist($depense);
            $solde = new Soldes();
            $montant_depense = $form->get("monDep")->getData();
            /*$montant = $soldesRepository->findBy(['agent' => $user],['id' => 'DESC']);*/

            /*if(count($montant)){
                $montant = $montant[0]->getMontant();
            } else{
                $montant = 0;
            }*/

            $soldes_agent = $userRepository->amountAgent($agent_email);

            $mont_recu =  $soldes_agent[0]['monRecu'] != null ? $soldes_agent[0]['monRecu'] : 0;
            $mont_depot =  $soldes_agent[0]['monDepot'] != null ? $soldes_agent[0]['monDepot'] : 0;
            $mont_depense = $soldes_agent[0]['monDep'] != null ? $soldes_agent[0]['monDep'] : 0;

            $montant = $mont_recu - $mont_depot - $mont_depense; 

            if($montant < $montant_depense){
                $this->addFlash("danger","Votre depense doit etre inferieur au solde");

                return $this->redirectToRoute("solde_depende");
            }

            $montant -= $montant_depense; 

            $solde->setMontDepens($montant_depense);
            $solde->setAgent($user);
            $solde->setMontant($montant);
            $manager->persist($solde);

            $manager->flush();

            return $this->redirectToRoute('home');
        }
        return $this->render("FrontOffice/depense/solde.html.twig",[
            'form'=> $form->createView()
        ]);
    }
}
