<?php

namespace App\Entity;

use App\Repository\SoldesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SoldesRepository::class)
 */
class Soldes
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $montant = 0;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $montRecu = 0;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $montDepo = 0;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $montDepens = 0;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="soldes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $agent;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMontant(): ?float
    {
        return $this->montant;
    }

    public function setMontant(float $montant): self
    {
        $this->montant = $montant;

        return $this;
    }

    public function getMontRecu(): ?float
    {
        return $this->montRecu;
    }

    public function setMontRecu(?float $montRecu): self
    {
        $this->montRecu = $montRecu;

        return $this;
    }

    public function getMontDepo(): ?float
    {
        return $this->montDepo;
    }

    public function setMontDepo(?float $montDepo): self
    {
        $this->montDepo = $montDepo;

        return $this;
    }

    public function getMontDepens(): ?float
    {
        return $this->montDepens;
    }

    public function setMontDepens(?float $montDepens): self
    {
        $this->montDepens = $montDepens;

        return $this;
    }

    public function getAgent(): ?User
    {
        return $this->agent;
    }

    public function setAgent(?User $agent): self
    {
        $this->agent = $agent;

        return $this;
    }
}
