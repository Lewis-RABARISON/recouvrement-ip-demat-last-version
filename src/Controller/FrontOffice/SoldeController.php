<?php

namespace App\Controller\FrontOffice;

use DateTime;
use App\Repository\SoldesRepository;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SoldeController extends AbstractController
{
    /**
     * @Route("/accueil", name="home")
     */
    public function index(SoldesRepository $soldesRepository, UserRepository $userRepository): Response
    {

        if(!$this->getUser())
            {
                return $this->redirectToRoute('app_login');
            }

            if($this->isGranted('ROLE_ADMIN')){

                return $this->redirectToRoute('dashboard');
    
            }else{

        $agent = $this->getUser();
        $agent_email = $agent->getEmail();
       
        /*$montant_recu = [];
        $montant_depot = [];
        $montant_depense = [];*/

        $soldes_agent = $userRepository->amountAgent($agent_email);
        // dd($soldes_agent);

        // $soldes_agent = $soldesRepository->findBy(['agent' =>$agent]);
        // $montant = $soldesRepository->findBy(['agent' => $agent],['id' => 'DESC']);

        $mont_recu = $soldesRepository->getSommesRecuByAgent($agent);
        if($mont_recu == 0){
            $mont_recu = 0;
        }else{
            $mont_recu = $soldesRepository->getSommesRecuByAgent($agent)["montRecu"];
        }

        /*if(count($montant)){
            $montant = $montant[0]->getMontant();
        } else{
            $montant = 0;
        }*/

        /*foreach($soldes_agent as $solde){
            $montant_recu[] = $solde->getMontRecu();
            $montant_depot[] = $solde->getMontDepo();
            $montant_depense[] = $solde->getMontDepens();
        }*/

        /*$montRecu = array_sum($montant_recu);
        $montDepot = array_sum($montant_depot);
        $montDepens = array_sum($montant_depense);*/

        //return $this->render('FrontOffice/accueil/index.html.twig', compact('mont_recu','montDepot','montDepens','montant','montRecu'));
        return $this->render('FrontOffice/accueil/index.html.twig', compact('mont_recu','soldes_agent'));
    }
}
}
